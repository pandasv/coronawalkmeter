package com.panda.plagueutils.coronawalkmeter;

public class CoronaConstants {

    public static final String PREF_KEY = "LOCATION_PREF";
    public static final String LATITUDE_KEY = "LAT";
    public static final String LONGITUDE_KEY = "LON";
    public static final String PROVIDER_KEY = "PRV";
    public static final String RADIUS_KEY = "RAD";
    public static final int INCREMENT_METERS = 25;
    public static final double DEF_ZOOM_NO_LOCATION = 10d;
    public static final double DEF_ZOOM_WITH_LOCATION = 19d;
}
