package com.panda.plagueutils.coronawalkmeter;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polygon;

import java.io.File;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements LocationListener {

//    private static final String TAG = "MAPACTIVITY";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 888;
    public static final int RECHECK_DELAY = 5000;

    private int radius = 100;
    private GeoPoint currentOwnLocation = null;
    private Marker selectedLocationMarker = null;
    private Marker currentPositionMarker = null;
    private Polygon radiusMarker = null;

    private MapView map = null;
    private LocationManager locationManager;
    private Handler handler = new Handler();

    private int permissionRequests = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        Configuration.getInstance().setUserAgentValue(getPackageName());
        Configuration.getInstance().setOsmdroidBasePath(new File(Environment.getExternalStorageDirectory(), "osmdroid"));
        Configuration.getInstance().setOsmdroidTileCache(new File(Environment.getExternalStorageDirectory(), "osmdroid/tiles"));
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string

        //inflate and create the map
        setContentView(R.layout.activity_main);

        radius = loadRadius();

        final TextView textView = findViewById(R.id.text);
        textView.setText(getDistanceString());

        SeekBar seekBar = findViewById(R.id.seek_bar);
        seekBar.setProgress((radius / CoronaConstants.INCREMENT_METERS)-1);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radius = (progress + 1) * CoronaConstants.INCREMENT_METERS;
                textView.setText(getDistanceString());
                Location savedLocation = loadLocation();
                if(savedLocation != null){
                    GeoPoint startPoint = new GeoPoint(savedLocation);
                    selectLocation(startPoint);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        initMap();

        final Button button = findViewById(R.id.start_button);
        button.setText(getString(isMyServiceRunning(DistanceService.class) ? R.string.stop : R.string.start));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMyServiceRunning(DistanceService.class)){
                    stopService();
                    button.setText(getString(R.string.start));
                }else{
                    startService();
                    button.setText(getString(R.string.stop));
                }
            }
        });
    }

    private String getDistanceString(){
        return getString(R.string.distance).concat(String.valueOf(radius));
    }

    private Location loadLocation(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(CoronaConstants.PREF_KEY, 0);
        String lat = sharedPreferences.getString(CoronaConstants.LATITUDE_KEY, null);
        String lon = sharedPreferences.getString(CoronaConstants.LONGITUDE_KEY, null);
        Location location = null;
        if (lat != null && lon != null) {
            String provider = sharedPreferences.getString(CoronaConstants.PROVIDER_KEY, null);
            location = new Location(provider);
            location.setLatitude(Double.parseDouble(lat));
            location.setLongitude(Double.parseDouble(lon));
        }
        return location;
    }

    private int loadRadius(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(CoronaConstants.PREF_KEY, 0);
        return sharedPreferences.getInt(CoronaConstants.RADIUS_KEY, 100);
    }

    private void saveLocation(GeoPoint location){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(CoronaConstants.PREF_KEY, 0);
        sharedPreferences.edit().putString(CoronaConstants.LATITUDE_KEY, String.valueOf(location.getLatitude())).apply();
        sharedPreferences.edit().putString(CoronaConstants.LONGITUDE_KEY, String.valueOf(location.getLongitude())).apply();
//        sharedPreferences.edit().putString(CoronaConstants.PROVIDER_KEY, location.getProvider()).apply();
        sharedPreferences.edit().putInt(CoronaConstants.RADIUS_KEY, radius).apply();
    }

    private void initMap() {
        map = findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        final IMapController mapController = map.getController();
        initLocationManager();

        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                selectLocation(p);
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                return false;
            }
        };

        MapEventsOverlay overlayEvents = new MapEventsOverlay(mReceive);
        map.getOverlays().add(overlayEvents);

        map.post(new Runnable() {
            @Override
            public void run() {
                Location savedLocation = loadLocation();
                if(savedLocation != null){
                    mapController.setZoom(CoronaConstants.DEF_ZOOM_WITH_LOCATION);
                    GeoPoint startPoint = new GeoPoint(savedLocation);
                    selectLocation(startPoint);
                } else {
                    GeoPoint startPoint = new GeoPoint(32.0853, 34.7818);
                    mapController.setZoom(CoronaConstants.DEF_ZOOM_NO_LOCATION);
                    mapController.setCenter(startPoint);
                }
                notifyMapOnNewLocation();
            }
        });
    }

    private void selectLocation(GeoPoint p){
        if(map != null){
            Marker startMarker = new Marker(map);
            startMarker.setPosition(p);
            startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            startMarker.setIcon(getResources().getDrawable(R.drawable.start_place));
            startMarker.setTitle("Home");
            if(selectedLocationMarker != null){
                map.getOverlayManager().remove(selectedLocationMarker);
            }
            map.getOverlayManager().add(startMarker);
            selectedLocationMarker = startMarker;
            IMapController mapController = map.getController();
            mapController.setCenter(p);
            addRadius(p);
            map.invalidate();
            saveLocation(p);
        }
    }

    private void addRadius(GeoPoint pt){
        List<GeoPoint> circle = Polygon.pointsAsCircle(pt, radius);
        Polygon polygon = new Polygon(map){
            @Override
            public boolean onClickDefault(Polygon polygon, MapView mapView, GeoPoint eventPos) {
                return false;
            }
        };
        polygon.getOutlinePaint().setColor(Color.RED);
        polygon.getOutlinePaint().setStrokeWidth(2f);
        polygon.setPoints(circle);
        if(radiusMarker != null){
            map.getOverlayManager().remove(radiusMarker);
        }
        map.getOverlayManager().add(polygon);
        radiusMarker = polygon;
    }

    private void markPosition(GeoPoint p){
        if(map != null){
            Marker positionMarker = new Marker(map);
            positionMarker.setPosition(p);
            positionMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            positionMarker.setIcon(getResources().getDrawable(R.drawable.your_place));
            positionMarker.setTitle(getString(R.string.you));
            if(currentPositionMarker != null){
                map.getOverlayManager().remove(currentPositionMarker);
            }
            map.getOverlayManager().add(positionMarker);
            currentPositionMarker = positionMarker;
        }
    }

    private void initLocationManager() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission();
                return;
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    private void showByeDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.no_location_permission));
        builder.setMessage(getString(R.string.location_permission_text));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(permissionRequests > 1){
                    finish();
                }else {
                    handler.removeCallbacksAndMessages(null);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            checkLocationPermission();
                        }
                    }, RECHECK_DELAY);
                }
            }
        });
        builder.create().show();
    }

    private void showBackDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.are_you_sure));
        builder.setMessage(getString(R.string.are_you_sure_text));
        builder.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopService();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Log.d(TAG, "onRequestPermissionsResult: PERMISSION_GRANTED");
                    handler.removeCallbacksAndMessages(null);
                    initLocationManager();
                } else {
//                    Log.d(TAG, "onRequestPermissionsResult: no permission yet");
                    showByeDialog();
                }
            }
        }
    }

    private void checkLocationPermission() {
        permissionRequests++;
        int permissionCheckFine = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheckCoarse = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheckFine != PackageManager.PERMISSION_GRANTED && permissionCheckCoarse != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onBackPressed() {
        showBackDialog();
    }

    public void onResume(){
        super.onResume();
        if(map != null) {
            map.onResume();
        }
    }

    public void onPause(){
        super.onPause();
        if(map != null) {
            map.onPause();
        }
    }

    private void notifyMapOnNewLocation(){
        if(map != null && currentOwnLocation != null){
            IMapController mapController = map.getController();
            mapController.setZoom(CoronaConstants.DEF_ZOOM_WITH_LOCATION);
            markPosition(currentOwnLocation);
            mapController.setCenter(currentOwnLocation);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentOwnLocation = new GeoPoint(location);
        notifyMapOnNewLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void startService() {
        Intent serviceIntent = new Intent(this, DistanceService.class);
//        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, serviceIntent);
    }
    public void stopService() {
        Intent serviceIntent = new Intent(this, DistanceService.class);
        stopService(serviceIntent);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
